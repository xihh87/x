" directories (`:help netrw`):
let g:netrw_banner = 0 " disable directory help
" [Use instead of NERDtree](https://shapeshed.com/vim-netrw/)
let g:netrw_liststyle = 2
let g:netrw_browse_split = 0
" topleft details in `:tag g:netrw_preview`
let g:netrw_preview = 1
let g:netrw_alto = 0
" right spliting `:tag g:netrw_altv`
let g:netrw_altv = 1
let g:netrw_winsize = 15
let g:netrw_chwin = 2 " use file window to open files
let g:netrw_chgwin = 2 " use file window to open files
let g:netrw_list_hide = '\.spellcheck$,\.swp$,\.bak$,\.tmp$,\.DS_Store$'
" open files with this handler:
" g:netrw_browsex_viewer = 'xdg-open'

function! ProjectDrawer()
    Lexplore
    vertical resize 20
endfunction

nnoremap <F9> :call ProjectDrawer()<CR>
